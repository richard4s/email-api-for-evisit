const express = require('express');
const transporter = require('../../../config/mailConfig');
const nodemailer = require('nodemailer');
var mailerhbs = require('nodemailer-express-handlebars');


const router = express.Router();

router.get('/', (req, res) => {
    res.status(200).json({
        message: 'The main mailer runner!'
    });
})

//To notify all CSO's
router.post('/newRequest', (req, res) => {

    let verifyOptions = {
        from: 'Alerts <alerts@cscs.ng>', // sender address
        to: req.body.email, // list of receivers
        subject: 'New eVisit Request', // Subject line
        template: 'newRequest', //Name email file template
            context: { // pass variables to template
                csoName: req.body.csoName,
                empName: req.body.empName
            }
      };

      const transporter1 = nodemailer.createTransport({
        // host: "smtp.office365.com",
        // port: 587,
        service: "Outlook365",
        auth: {
                user: 'alerts@cscs.ng',
                pass: 'AlertPass123%'
            }
        // tls: {
        //   // do not fail on invalid certs
        //   rejectUnauthorized: false
        // }
    });

      const handlebarOptions = {
        viewEngine: {
          extName: '.hbs',
          partialsDir: './templates',
          layoutsDir: './templates',
          defaultLayout: 'newRequest.hbs',
        },
        viewPath: './templates',
        extName: '.hbs',
      };
      
      transporter1.use('compile', mailerhbs(handlebarOptions));
    
      transporter1.sendMail(verifyOptions, function (err, info) {
        if(err)
          console.log(err)
        else
          console.log(info);
      }, function (err, response) {
        if (err) {
            res.status(400).json({
                message: 'Error send email, please contact administrator for best support.'
            });
        }
        // res.send('Email send successed to you email' + req.body.email + '.');
        res.status(200).json({
            message: 'Mail Sent!'
        });
        done(err, 'done');
    });

      res.status(200).json({
        message: 'Mail Sent!'
    });
})


//To notify the visitor's
router.post('/notifyVisitors', (req, res) => {

    let verifyOptions = {
        from: 'Alerts <alerts@cscs.ng>', // sender address
        to: req.body.email, // list of receivers
        subject: 'Your eVisit Request Details', // Subject line
        template: 'notifyVisitors', //Name email file template
            context: { // pass variables to template
                empName: req.body.empName,
                name: req.body.name,
                code: req.body.code,
                date: req.body.date,
                time: req.body.time,
                purpose: req.body.purpose
            }
      };

      const transporter2 = nodemailer.createTransport({
        service: "Outlook365",
        auth: {
                user: 'alerts@cscs.ng',
                pass: 'AlertPass123%'
            }
    });

      const handlebarOptions = {
        viewEngine: {
          extName: '.hbs',
          partialsDir: './templates',
          layoutsDir: './templates',
          defaultLayout: 'notifyVisitors.hbs',
        },
        viewPath: './templates',
        extName: '.hbs',
      };
      
      transporter2.use('compile', mailerhbs(handlebarOptions));
    
      transporter2.sendMail(verifyOptions, function (err, info) {
        if(err)
          console.log(err)
        else
          console.log(info);
      }, function (err, response) {
        if (err) {
            res.status(400).json({
                message: 'Error send email, please contact administrator for best support.'
            });
        }
        // res.send('Email send successed to you email' + req.body.email + '.');
        res.status(200).json({
            message: 'Mail Sent!'
        });
        done(err, 'done');
    });

      res.status(200).json({
        message: 'Mail Sent!'
    });
})


//To employeeRegistered
router.post('/employeeRegistered', (req, res) => {

    let verifyOptions = {
        from: 'Alerts <alerts@cscs.ng>', // sender address
        to: req.body.email, // list of receivers
        subject: 'Your eVisit Details', // Subject line
        template: 'employeeRegistered', //Name email file template
            context: { // pass variables to template
                name: req.body.name,
                code: req.body.code,
                date: req.body.date,
                time: req.body.time,
                purpose: req.body.purpose
            }
      };

      const transporter3 = nodemailer.createTransport({
        service: "Outlook365",
        auth: {
                user: 'alerts@cscs.ng',
                pass: 'AlertPass123%'
            }
    });

      const handlebarOptions = {
        viewEngine: {
          extName: '.hbs',
          partialsDir: './templates',
          layoutsDir: './templates',
          defaultLayout: 'employeeRegistered.hbs',
        },
        viewPath: './templates',
        extName: '.hbs',
      };
      
      transporter3.use('compile', mailerhbs(handlebarOptions));
    
      transporter3.sendMail(verifyOptions, function (err, info) {
        if(err)
          console.log(err)
        else
          console.log(info);
      }, function (err, response) {
        if (err) {
            res.status(400).json({
                message: 'Error send email, please contact administrator for best support.'
            });
        }
        // res.send('Email send successed to you email' + req.body.email + '.');
        res.status(200).json({
            message: 'Mail Sent!'
        });
        done(err, 'done');
    });

      res.status(200).json({
        message: 'Mail Sent!'
    });
})


//Weekly Report
router.post('/weeklyReport', (req, res) => {

  let verifyOptions = {
      from: 'Alerts <alerts@cscs.ng>', // sender address
      to: req.body.email, // list of receivers
      subject: 'eVisit Weekly Report', // Subject line
      template: 'weeklyReport', //Name email file template
          context: { // pass variables to template
              name: req.body.name,
              totalWeeklyVisits: req.body.totalWeeklyVisits,
              personalVisits: req.body.personalVisits,
              meetingVisits: req.body.meetingVisits,
              officialVisits: req.body.officialVisits,
              customerVisits: req.body.customerVisits
          }
    };

    const transporter4 = nodemailer.createTransport({
      service: "Outlook365",
      auth: {
              user: 'alerts@cscs.ng',
              pass: 'AlertPass123%'
          }
  });

    const handlebarOptions = {
      viewEngine: {
        extName: '.hbs',
        partialsDir: './templates',
        layoutsDir: './templates',
        defaultLayout: 'weeklyReport.hbs',
      },
      viewPath: './templates',
      extName: '.hbs',
    };
    
    transporter4.use('compile', mailerhbs(handlebarOptions));
  
    transporter4.sendMail(verifyOptions, function (err, info) {
      if(err)
        console.log(err)
      else
        console.log(info);
    }, function (err, response) {
      if (err) {
          res.status(400).json({
              message: 'Error send email, please contact administrator for best support.'
          });
      }
      // res.send('Email send successed to you email' + req.body.email + '.');
      res.status(200).json({
          message: 'Mail Sent!'
      });
      done(err, 'done');
  });

    res.status(200).json({
      message: 'Mail Sent!'
  });
})

module.exports = router;