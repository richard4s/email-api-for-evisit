const express = require('express');
const transporter = require('../../../config/mailConfig');


const router = express.Router();

// References /api/post
router.get('/', (req, res) => {
    res.status(200).json({
        message: 'Welcome  Message'
    });
})

router.get('/run', (req, res) => {

    let verifyOptions = {
        from: 'Alerts <alerts@cscs.ng>', // sender address
        to: 'richardoluwo50@gmail.com', // list of receivers
        subject: 'Test', // Subject line
        template: 'newRequest', //Name email file template
            context: { // pass variables to template
                hostUrl: 'req.headers.host',
                customeName: 'user.info.firstname +  + user.info.lastname',
                resetUrl: 'req.headers.hosoken',
                resetCode: 'token'
            }
      };
    
      transporter.sendMail(verifyOptions, function (err, info) {
        if(err)
          console.log(err)
        else
          console.log(info);
      }, function (err, response) {
        if (err) {
            res.status(400).json({
                message: 'Error send email, please contact administrator to best support.'
            });
        }
        // res.send('Email send successed to you email' + req.body.email + '.');
        res.status(200).json({
            message: 'Mail Sent!'
        });
        done(err, 'done');
    });

      res.status(200).json({
        message: 'Mail Sent!'
    });
})

module.exports = router;