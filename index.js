const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
// const port = 3000;

const app = express();
//var app = express.createServer();


app.use(bodyParser.json());
app.use(cors());

const testmail = require('./app/routes/api/testmail');
const mainRunner = require('./app/routes/api/main');
//app.use(express.staticProvider(__dirname + '/app/template'));

// app.get('/', (req, res) => res.render('index.html'));
app.use('/', testmail);
app.use('/api/testmail', testmail);
app.use('/api/mail/main', mainRunner);

app.listen(process.env.PORT || 3000, () => console.log(`Server started on port ${process.env.PORT}!`))
// app.listen(port, () => console.log(`Server started on port ${port}!`));