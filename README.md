# Default Endpoint
`/api/mail/main`
### GET Request
Just a basic GET request for testing

## Notify CSO Of New Visitors Request
 `/api/mail/main/newRequest`
### POST Request
### Fields
 - `email`
 - `csoName`
 - `empName`

## To Notify the Visitor that he has been requested
`/api/mail/main/notifyVisitors`
### POST Request
### Fields
 - `email`
 - `empName`
 - `name`
 - `date`
 - `time`
 - `purpose`
 - `code`


## To Notify the Employee who made the visit
`/api/mail/main/employeeRegistered`
### POST Request
### Fields
 - `email`
 - `name`
 - `date`
 - `time`
 - `purpose`
 - `code`

 ## To Send Weekly eVisit Reports
`/api/mail/main/weeklyReport`
### POST Request
### Fields
 - `email`
 - `name`
 - `totalWeeklyVisits`
 - `personalVisits`
 - `meetingVisits`
 - `officialVisits`
 - `customerVisits`
